var Home = new Vue({
    el : '#home',
    props : [],
    data () {
        return{
            menu : false,isOpen : false,cloud : false,note : false,map : false,tab : true, 
            bedrooms : [
                {id : 1, image : '../img/accomodation/bedroom-chandelier.jpg', imgdescription : 'Chandeliar', rating : 0, votes : 567, likes : 507, dislikes : 6, voted : false, },
                {id : 2, image : '../img/accomodation/desire.jpg', imgdescription : 'Desire', rating : 0, votes : 0, likes : 156, dislikes : 0, voted : false, },
                {id : 3, image : '../img/accomodation/view.jpg', imgdescription : 'Valley View', rating : 0, votes : 0, likes : 0, dislikes : 0, voted : false, },
                {id : 4, image : '../img/accomodation/kings.jpg', imgdescription : 'Sweet Moon', rating : 0, votes : 0, likes : 0, dislikes : 0, voted : false, },
                {id : 5, image : '../img/accomodation/living-bed.jpg', imgdescription : 'Chamber', rating : 0, votes : 0, likes : 0, dislikes : 0, voted : false, },
                {id : 6, image : '../img/accomodation/workspace-bed.jpg', imgdescription : 'Wrest Space', rating : 0, votes : 0, likes : 0, dislikes : 0, voted : false, },
            ],
            spa : [
                {image : '../img/spa/beach.jpg', imgdescription : 'White Beaches'},
                {image : '../img/spa/drinks.jpeg', imgdescription : 'Lovely Cheers'},
                {image : '../img/spa/toweled.jpg', imgdescription : 'Face Cleaning'},
                {image : '../img/spa/masked.jpg', imgdescription : 'Fruit Masking'},
                {image : '../img/spa/pool.jpg', imgdescription : 'Water Massage'},
                {image : '../img/spa/sleeping.jpg', imgdescription : 'Well Relaxed'},           
            ],
            treatbeds : [
                {id : 1, image : '../img/accomodation/bedroom-chandelier.jpg', imgdescription : 'Chandeliar',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 175, rating : 4, },
                {id : 2, image : '../img/accomodation/desire.jpg', imgdescription : 'Desire',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 134, rating : 4, },
                {id : 3, image : '../img/accomodation/view.jpg', imgdescription : 'Valley View',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 254, rating : 3.5, },
                {id : 4, image : '../img/accomodation/kings.jpg', imgdescription : 'Sweet Moon',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 149, rating : 4, },
                {id : 5, image : '../img/accomodation/living-bed.jpg', imgdescription : 'Chamber',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 94, rating : 4, },
                {id : 6, image : '../img/accomodation/workspace-bed.jpg', imgdescription : 'Wrest Space',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 135, rating : 3, },
                {id : 7, image : '../img/accomodation/south.jpg', imgdescription : 'Southside Bedroom',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 125, rating : 4, },
                {id : 8, image : '../img/accomodation/sibling.jpg', imgdescription : 'Twin Bed',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 84, rating : 4, },
                {id : 8, image : '../img/accomodation/white-wooden.jpg', imgdescription : 'White Wooden',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 34, rating : 4, },
                // {id : 8, image : '../img/accomodation/sibling.jpg', imgdescription : 'Twin Bed',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 34, rating : 4, },
                // {id : 8, image : '../img/accomodation/sibling.jpg', imgdescription : 'Twin Bed',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 34, rating : 4, },
                // {id : 8, image : '../img/accomodation/sibling.jpg', imgdescription : 'Twin Bed',priceinCents : 190000, formatedPrice : '$ 1,900.00', reviewCount : 34, rating : 4, },
            ],
            tours : [
                {image: '../img/tours/eiffel-tower.jpg', imagesec : '../img/tours/eiffel-2.jpg', imgdescription : 'Eiffel Tower, Paris', love : 35,content:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid reiciendis voluptatum, doloremque libero beatae animi maxime perspiciatis molestias asperiores',},
                {image: '../img/tours/pissa-2.jpg', imagesec : '../img/tours/pissa-1.jpg', imgdescription : 'Tower of Pissa, Italy', love : 27,content:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid reiciendis voluptatum, doloremque libero beatae animi maxime perspiciatis molestias asperiores',},
                {image: '../img/tours/pyramids-hero.jpg', imagesec : '../img/tours/pyramids-2.jpg', imgdescription : 'Pyramids, Egypt', love : 18,content:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid reiciendis voluptatum, doloremque libero beatae animi maxime perspiciatis molestias asperiores',}
            ],
            api_key : 'be82bd301ff4315c56d59dd7e9577ec9',
            url_base : 'https://api.openweathermap.org/data/2.5/',
            query : '',
            weather : {},
        } 
    },
    methods : {
        fetchWeather(){
            fetch(`${this.url_base}weather?q=${this.query}&units=metric&APPID=${this.api_key}`).then(response => response.json()).then(this.setResult);  
        },
        fetchWeathertwo(e){
            if (e.key == 'Enter') {
                fetch(`${this.url_base}weather?q=${this.query}&units=metric&APPID=${this.api_key}`).then(response => response.json()).then(this.setResult);
            }
        },
        datebuilder(){
            let d = new Date();
            let months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
            let days = ['Sunday', 'Monday', 'Teusday', 'Wednesday', 'Thursday','Friday', 'Saturday'];

            let day = days[d.getDay()];
            let date = d.getDate();
            let month = months[d.getMonth()];
            let year = d.getFullYear();

            return `${day} ${date} ${month}, ${year}`;
        },
        getIcon(icon){
            let link = 'https://openweathermap.org/img/wn/'+icon+'@2x.png';
            return `${link}`;
        },
        setResult(result){
            this.weather = result;
        },
        votelike(id) {
            const room = this.bedrooms.find((item) => {
                return item.id === id
            });

            room.votes += 1 ;
            room.likes += 1 ;
        },
        votedislike(id) {
            const room = this.bedrooms.find((item) => {
                return item.id === id
            });
            room.votes += 1 ;
            room.dislikes += 1
        },
        pushVote(id){
            this.newBedrooms.push(this.bedrooms[id]);
            console.log(this.newBedrooms.length);
        },
    },
    mounted: function () {
        const handleEscape = (event) => {
            if (event.key == 'Esc' || event.key == 'Escape') {
                this.open = false;
            }
        }
        document.addEventListener('keydown', handleEscape);

        this.$once('hook:beforeDestroy', () => {
            document.removeEventListener('keydown', handleEscape);
        });

        document.addEventListener('scroll', () => {
            let header = document.querySelector('header')
            let navlinks = document.querySelectorAll('.nav-link')
            let navpages = document.querySelectorAll('.nav-pages')

            if (window.scrollY > 0) {
                header.classList.remove('text-pink-400','bg-gray-800')
                header.classList.add('header-active',)
                navlinks.forEach((event) => {
                    event.classList.remove('hover:text-gray-800', 'hover:bg-pink-400',)
                    event.classList.add('hover:bg-gray-800', 'hover:text-pink-400',)
                });

            } else {
                navlinks.forEach((event) => {
                    header.classList.add('text-pink-400', 'bg-gray-800')
                    event.classList.remove('hover:bg-gray-800', 'hover:text-pink-400',)
                    event.classList.add('hover:text-gray-800', 'hover:bg-pink-400',)
                }); 
            }
        });  
    }
});

// navlinks.forEach((tab,tabindex)=>{
//     tab.addEventListener('click',()=>{
//         navlinks.forEach((tab)=>{
//             tab.classList.remove('text-gray-800', 'bg-pink-400',)
//         })
//         tab.classList.add('bg-gray-800', 'text-pink-400',)

//         navpages.forEach((content,contentindex)=>{
//             if (tabindex == contentindex) {
//                 content.style.display = 'block'
//             }else{
//                 content.style.display = 'none'
//             }
//         })
//     })
// })
    


