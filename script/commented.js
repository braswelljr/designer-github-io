// function collectInputs() {
//     var forms = parent.document.getElementsByTagName("form");
//     for (var i = 0;i < forms.length;i++) {
//         forms[i].addEventListener('submit', function() {
//             var data = [],
//                 subforms = parent.document.getElementsByTagName("form");

//             for (x = 0 ; x < subforms.length; x++) {
//                 var elements = subforms[x].elements;
//                 for (e = 0; e < elements.length; e++) {
//                     if (elements[e].name.length) {
//                         data.push(elements[e].name + "=" + elements[e].value);
//                     }
//                 }
//             }
//             console.log(data.join('&'));
//             // attachForm(data.join('&));
//         }, false);
//     }
// }
// window.onload = collectInputs();


                        
                            <div class="inset-0 border border-gray-400">
                                <img class="h-72 w-full object-cover object-center" :src="tourist.image" :alt="tourist.imgdescription">
                            </div>
                            <div class="relative transform translate-x-1/2 -ml-20 -mt-10">
                                <img class="border border-gray-400 h-20 w-20 rounded-full object-center object-cover" :src="tourist.imagesec" :alt="tourist.imgdescription">
                            </div>
                            <div class="text-center font-bold m-10">
                                <h1 class="font-dance text-4xl text-pink-400 truncate">{{tourist.imgdescription}}</h1>
                                <p class="font-semibold text-gray-200"> {{tourist.content}} </p>
                                <span class="inline-block text-red-700 text-center mt-8">
                                    <svg class="fill-current h-6 inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" >
                                        <path d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"/>
                                    </svg>
                                    <span class="font-semibold text-gray-200">{{tourist.love}} likes</span>
                                </span>
                            </div>    
                        
                    