//navbar change colour on scroll
window.addEventListener('scroll',() => {
    let header = document.querySelector('header');
    let navlinks = document.querySelectorAll('.nav-link');
    let alert = document.querySelector('#welcom-alert');
    
    if (window.scrollY > 0) {
        header.classList.remove('text-pink-400', );
        header.classList.remove('bg-gray-800',);
        header.classList.add('header-active',);
        navlinks.forEach( (event) => {
            event.classList.remove('hover:text-gray-800','hover:bg-pink-400',);
            event.classList.add('hover:bg-gray-800','hover:text-pink-400',);
        });  
    }  
    else{
        navlinks.forEach( (event) => { 
            event.classList.remove('hover:bg-gray-800','hover:text-pink-400',);
            event.classList.add('hover:text-gray-800','hover:bg-pink-400',);
        });
        header.classList.add('text-pink-400','bg-gray-800');
    }

    //on scroll it should be fixed
});

//welcome alert
window.addEventListener('load',() => {
    let alert = document.get
});
